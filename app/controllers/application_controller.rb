class ApplicationController < ActionController::API
  include Pundit
  respond_to :json
  before_action :set_default_response_format
  rescue_from ActiveRecord::RecordNotDestroyed, with: :not_destroyed

  private

  def not_destroyed(error)
    render json: { errors: error.record.errors }, status: :unprocessable_entity
  end

  def set_default_response_format
    request.format = :json
  end

  def render_jsonapi_response(resource)
    if resource.errors.empty?
      render jsonapi: resource
    else
      render jsonapi_errors: resource.errors, status: 400
    end
  end
end
