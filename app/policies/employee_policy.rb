class EmployeePolicy < ApplicationPolicy
  attr_reader :user, :employee

  def initialize(user, employee)
    raise Pundit::NotAuthorizedError, 'must be logged in' unless user

    @user = user
    @employee = employee
  end

  def update?
    user.has_role?(:admin)
  end
end
