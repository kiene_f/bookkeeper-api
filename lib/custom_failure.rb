module CustomFailure
  class CustomAuthFailure < Devise::FailureApp
    def respond
      request.format.json? ? api_response : super
    end

    private

    def api_response
      self.status = 401
      self.content_type = 'application/json'
      self.response_body = { 'errors' => ['Invalid login credentials'] }.to_json
    end
  end
end
