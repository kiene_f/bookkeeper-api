# == Schema Information
#
# Table name: employees
#
#  id         :bigint           not null, primary key
#  first_name :string
#  last_name  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_employees_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe Employee, type: :model do
  before(:each) do
    @user = FactoryBot.create(:user)
  end

  it 'has a user' do
    employee = build(:employee, user: nil)

    expect(employee).to_not be_valid

    employee.user = @user
    expect(employee).to be_valid
  end
  it 'has a first_name' do
    employee = build(:employee, user: @user, first_name: '')

    expect(employee).to_not be_valid

    employee.first_name = Faker::Name.first_name
    expect(employee).to be_valid
  end
  it 'has a last_name' do
    employee = build(:employee, user: @user, last_name: '')

    expect(employee).to_not be_valid

    employee.last_name = Faker::Name.last_name
    expect(employee).to be_valid
  end

  it 'has a first_name between 2 and 50 characters' do
    employee = build(:employee, user: @user, first_name: Faker::Lorem.characters(number: 1))

    expect(employee).to_not be_valid

    employee.first_name = Faker::Lorem.characters(number: 2)
    expect(employee).to be_valid

    employee.first_name = Faker::Lorem.characters(number: 50)
    expect(employee).to be_valid
  end

  it 'has a last_name between 2 and 50 characters' do
    employee = build(:employee, user: @user, last_name: Faker::Lorem.characters(number: 1))

    expect(employee).to_not be_valid

    employee.last_name = Faker::Lorem.characters(number: 2)
    expect(employee).to be_valid

    employee.last_name = Faker::Lorem.characters(number: 50)
    expect(employee).to be_valid
  end
end
