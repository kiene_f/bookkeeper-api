require 'rails_helper'

describe EmployeePolicy do
  before(:each) do
    @user = FactoryBot.create(:user)
  end

  subject { described_class.new(user, employee) }

  let(:employee) do
    build(:employee, user: @user)
  end

  it "should raise an exception when users aren't logged in" do
    expect do
      EmployeePolicy.new(nil, employee)
    end.to raise_error(Pundit::NotAuthorizedError)
  end

  context 'being an moderator' do
    let(:role) { Role.create(name: 'moderator') }
    let(:user) { FactoryBot.create(:user, role_ids: [role.id]) }

    it { is_expected.to forbid_action(:update) }
  end

  context 'being an administrator' do
    let(:role) { Role.create(name: 'admin') }
    let(:user) { FactoryBot.create(:user, role_ids: [role.id]) }

    it { is_expected.to permit_actions(:update) }
  end
end
