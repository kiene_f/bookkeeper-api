# == Schema Information
#
# Table name: employees
#
#  id         :bigint           not null, primary key
#  first_name :string
#  last_name  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_employees_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :employee, class: Employee do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    user
  end
end
