require 'rails_helper'

RSpec.describe Api::V1::Users::SessionsController, type: :request do
  let(:user) { create_user }
  let(:login_url) { '/api/v1/login' }
  let(:logout_url) { '/api/v1/logout' }

  context 'when logging in' do
    before do
      login_with_api(user)
    end

    it 'returns a token' do
      expect(response.headers['Authorization']).to be_present
    end

    it 'returns 200' do
      expect(response.status).to eq(200)
    end
  end

  context 'when password is missing' do
    before do
      post login_url, params: {
        user: {
          email: user.email,
          password: nil
        }
      }
    end

    it 'returns 401' do
      expect(response.status).to eq(401)
    end
  end
end
