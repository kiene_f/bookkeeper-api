require 'rails_helper'

RSpec.describe Api::V1::EmployeesController, type: :request do
  let(:user) { create_user }

  before(:each) do
    login_with_api(user)
  end

  let(:valid_attributes) do
    FactoryBot.build(:employee, user: user).attributes
  end

  let(:invalid_attributes) do
    {
      id: 'a',
      first_name: Faker::Lorem.characters(number: 1),
      last_name: Faker::Lorem.characters(number: 1)
    }
  end

  let(:valid_headers) do
    {
      'Authorization': response.headers['Authorization']
    }
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      Employee.create! valid_attributes
      get api_v1_employees_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      employee = Employee.create! valid_attributes
      get api_v1_employee_url(employee), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Employee' do
        expect do
          post api_v1_employees_url,
               params: { employee: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Employee, :count).by(1)
      end

      it 'renders a JSON response with the new employee' do
        post api_v1_employees_url,
             params: { employee: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Employee' do
        expect do
          post api_v1_employees_url,
               params: { employee: invalid_attributes }, as: :json
        end.to change(Employee, :count).by(0)
      end

      it 'renders a JSON response with errors for the new employee' do
        post api_v1_employees_url,
             params: { employee: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) do
        {
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name
        }
      end

      it 'updates the requested employee' do
        employee = Employee.create! valid_attributes
        patch api_v1_employee_url(employee),
              params: { employee: new_attributes }, headers: valid_headers, as: :json
        employee.reload
        new_attributes.each_pair do |key, value|
          expect(employee[key]).to eq(value)
        end
      end

      it 'renders a JSON response with the employee' do
        employee = Employee.create! valid_attributes
        patch api_v1_employee_url(employee),
              params: { employee: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'renders a JSON response with errors for the employee' do
        employee = Employee.create! valid_attributes
        patch api_v1_employee_url(employee),
              params: { employee: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested employee' do
      employee = Employee.create! valid_attributes
      expect do
        delete api_v1_employee_url(employee), headers: valid_headers, as: :json
      end.to change(Employee, :count).by(-1)
    end
  end
end
