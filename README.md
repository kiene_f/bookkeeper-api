# Bookkeeper API

[![Live Demo](https://img.shields.io/badge/demo-online-green.svg)](https://rocky-mountain-07042.herokuapp.com/)

Bookkeeper is a simple Rails API with JWT Authentification

## Table of Contents

- [Dependencies](#dependencies)
- [Demo](#demo)
- [Usage](#usage)
- [Project Setup](#project-setup)

## Dependencies

- Ruby 3.0.2
- Rails 6.1.4
- PostgreSQL
- Devise
- Rolify
- Rspec
- FactoryBot
- Faker

## Demo

Demo on Heroku:

- [Production](https://rocky-mountain-07042.herokuapp.com/)
- [Development](https://quiet-fortress-31648.herokuapp.com/)

## Usage

| HTTP verbs | Paths                          |               Used for |
| ---------- | ------------------------------ | ---------------------: |
| POST       | api/v1/signup                  |          Create a user |
| POST       | api/v1/login                   |    Authenticate a user |
| DELETE     | api/v1/logout                  |          Logout a user |
| GET        | /api/v1/employees              |     List all employees |
| GET        | /api/v1/employees/:employee_id | Show a single employee |
| POST       | /api/v1/employees              |     Create an employee |
| PUT        | /api/v1/employees/:employee_id |     Update an employee |
| DELETE     | /api/v1/employees/:employee_id |     Delete an employee |

## Project Setup

**Install all gems**:

```console
$ bundle install
```

**Update the database with new data model**:

```console
$ rails db:migrate
```

**Start the web server on `http://localhost:3000` by default**:

```console
$ rails server
```

**Run all RSpec tests**:

```console
$ rspec
```
